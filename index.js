import express from "express"
// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
const firebaseConfig = {
  apiKey: "AIzaSyCMObs2TsqvWkKbi5hpoKNkwGjF2jNt9lE",
  authDomain: "openmarket-xd777.firebaseapp.com",
  databaseURL: "https://openmarket-xd777-default-rtdb.firebaseio.com",
  projectId: "openmarket-xd777",
  storageBucket: "openmarket-xd777.appspot.com",
  messagingSenderId: "318336604623",
  appId: "1:318336604623:web:1334989b77dfb9990d1aba",
};
import { collection, query, where, getFirestore, getDocs} from "firebase/firestore";

const app = initializeApp(firebaseConfig);
const db = getFirestore();
const announcementsRef = collection(db, "Announcements");
const expressApp = express()
const port = 2999
const __dirname = process.env.PWD

expressApp.use(express.static(__dirname));

expressApp.get('/', async (req, res) => {
  res.sendFile('/getUserAds.html', { root: __dirname })
})


expressApp.post('/api/getUserAds', async (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('X-Powered-By', 'PHP')
  var userId = +req.query.userId  
  let results = []
  const q = query(announcementsRef, where("UserId", "==", userId));
  const querySnapshot = await getDocs(q);
  querySnapshot.forEach((doc) => {
    // doc.data() is never undefined for query doc snapshots
    results.push(doc.data())
    //console.log(results)
  });
  res.json(results)
})

expressApp.listen(port, () => {
  console.log(`https://localhost:${port}`)
})
